package traductor;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.swing.JPanel;
import java.awt.Font;
import java.awt.Color;

public class Principal {

	private JFrame frame;
	private JTextField palabra;
	private JLabel traduccion;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton Btraductor = new JButton("Traducir");
		Btraductor.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				Document document;

				String webPage = "https://www.spanishdict.com/traductor/" + palabra.getText();

				try {
					document = Jsoup.connect(webPage).get();
					Elements palabra = document.getElementById("quickdef1-es").getElementsByClass("_1btShz4h");

					traduccion.setText(palabra.get(0).html());

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		Btraductor.setBounds(129, 221, 182, 29);
		frame.getContentPane().add(Btraductor);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 48, 182, 162);
		frame.getContentPane().add(panel);
				panel.setLayout(null);
		
				JLabel lblNewLabel = new JLabel("  ESPA\u00D1OL");
				lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
				lblNewLabel.setBounds(53, 0, 89, 50);
				panel.add(lblNewLabel);
				
						palabra = new JTextField();
						palabra.setForeground(new Color(0, 0, 0));
						palabra.setBounds(10, 41, 162, 110);
						panel.add(palabra);
						palabra.setColumns(10);
						
						JPanel panel_1 = new JPanel();
						panel_1.setBounds(242, 48, 182, 162);
						frame.getContentPane().add(panel_1);
						panel_1.setLayout(null);
						
								traduccion = new JLabel("");
								traduccion.setFont(new Font("Tahoma", Font.BOLD, 14));
								traduccion.setBounds(10, 44, 162, 107);
								panel_1.add(traduccion);
								traduccion.setForeground(Color.BLACK);
								
										JLabel lblNewLabel_1 = new JLabel("INGLES");
										lblNewLabel_1.setBounds(66, 13, 69, 20);
										panel_1.add(lblNewLabel_1);
										lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
										
										JLabel lblNewLabel_2 = new JLabel("   Traductor Espa\u00F1ol a Ingl\u00E9s");
										lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 15));
										lblNewLabel_2.setBounds(91, 11, 220, 29);
										frame.getContentPane().add(lblNewLabel_2);
	}
}
