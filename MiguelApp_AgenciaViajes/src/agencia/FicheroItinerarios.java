package agencia;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class FicheroItinerarios {

	private ArrayList<String> lectura;

	private boolean cerrar_fichero = false;// true si cierro el fichero de escritura, false si cierro el fichero de
											// lectura

	private File fichero;

	private FileWriter ficheroW_sinbuffer;
	private BufferedWriter ficheroW_conbuffer;

	private FileReader ficheroR_sinbuffer;
	private BufferedReader ficheroR_conbuffer;

	private int numero_lineas = 0;

	
	/**********************************************************/

	public boolean Abrir(boolean escritura_lectura,String nombre) throws IOException {
		
		// crea el fichero si no existe, se creaara vacio, si existe,abrira el fichero
		
		fichero = new File(nombre);

		try {
			if (!fichero.exists()) {

				fichero.createNewFile();// genera el fichero vacio

				System.out.println("El fichero no existia, por tanto se creó");

				return false;
			} else {

				if (escritura_lectura == true) {// si escritura_lectura vale true se abre el fichero para escribir
					ficheroW_sinbuffer = new FileWriter(fichero, true);
					ficheroW_conbuffer = new BufferedWriter(ficheroW_sinbuffer);
					cerrar_fichero = true;

				} else {// se abre el fichero para leer

					ficheroR_sinbuffer = new FileReader(fichero);
					ficheroR_conbuffer = new BufferedReader(ficheroR_sinbuffer);
					// System.out.println("Abriste el fichero en modo Lectura"); esto en la funcion de CL menu leer

				}

			}

		} catch (IOException e) {

			System.out.println(e.getMessage());

		}
		return true;
	}

	public Itinerario Leer() throws IOException {

		int lineas_para_leer;

		lectura = new ArrayList<String>();

		if (Final() == false) {

			lectura.add(ficheroR_conbuffer.readLine());// 1º posicion nombre itinerario

			lineas_para_leer = Integer.parseInt(ficheroR_conbuffer.readLine());// leyendo numero destinos
			lectura.add(Integer.toString(lineas_para_leer));// guardamos nº destinos en 2º posicion

			for (int i = 0; i < lineas_para_leer; i++) {// guardamos en las demas posiciones los destinos
				lectura.add(ficheroR_conbuffer.readLine());

			}
							
		} else {

			System.out.println("Se ha llegado al final del fichero");

		}
		Itinerario itinerario =new Itinerario(lectura);
		return itinerario;
	}
	

	public void Escribir(ArrayList<Itinerario> Datos) throws IOException {

		if (Abrir(true,"Itinerarios.txt")) {

			for (int i = 0; i < Datos.size(); i++) 
				for (int a = 0; a < Datos.get(i).getItinerario().size(); a++) { 
				ficheroW_conbuffer.write(Datos.get(i).getItinerario().get(a));
				ficheroW_conbuffer.newLine();
				numero_lineas++;

			}

			ficheroW_conbuffer.flush();
		}
	}

	public void Cerrar() throws IOException {

		try {
			if (cerrar_fichero == true) {

				ficheroW_conbuffer.close();

			} else {

				ficheroR_conbuffer.close();

			}
		} catch (NullPointerException e) {
			throw new IOException("El fichero no se encuentra abierto");
		}
	}

	public boolean Final() throws IOException {

		return !ficheroR_conbuffer.ready();// el metodo .ready() devuelve true si queda lineas para leer, asique lo niego
	}

	public void Vaciar() throws IOException {
		Abrir(true,"Itinerarios.txt");
		Cerrar();
		ficheroW_sinbuffer = new FileWriter(fichero);
		ficheroW_conbuffer = new BufferedWriter(ficheroW_sinbuffer);

		for (int i = 0; i < numero_lineas; i++) {
			ficheroW_conbuffer.write(" ");
			ficheroW_conbuffer.newLine();
		}

	}

}
