package agencia;

import java.util.ArrayList;

public class Itinerario {

	
	private ArrayList<String> datos_fichero = new ArrayList<>();// array que va a contener los datos de los itinerarios, cuando esten cargados en memoria

	public Itinerario(ArrayList<String> it) {
		datos_fichero = it;

	}

	public ArrayList<String> getItinerario() {
		return datos_fichero;
	}

	public void setItinerario(ArrayList<String> itinerario) {
		this.datos_fichero = itinerario;
	}


	public String toString() {
		return "Itinerario [" + datos_fichero + "]";
	}

}
