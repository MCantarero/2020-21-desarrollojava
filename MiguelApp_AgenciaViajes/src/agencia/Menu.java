package agencia;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Menu {
	//@author Miguel Ángel Cantarero Ortega

	private FicheroItinerarios fichero = new FicheroItinerarios();// creamos el objeto tipo fichero itinerarios para
																	// hacer uso de los metodos de manipulacion del
																	// fichero

	private ArrayList<Itinerario> Array_Itinerarios = new ArrayList<Itinerario>(); // este va a ser el array principal
																					// del programa que guardara objetos
																					// de tipo Itinerario

	

	public Menu() {

	}

	/*******************************************/
	public int opcion_seleccionada(Scanner teclado) {

		int op;
		System.out.println(
				"\n                          BIENVENIDO A NUESTRA AGENCIA DE VIAJES \n\n"
				+" Escriba el numero de opcion que desee usar \n"
				+ " **************************************************************************************************\n"
				+ " *  1 Ler datos del fichero y cargarlo en memoria       2 Añadir un itinerario a memoria 	  *\n"
				+ " *  3 Borrar un itinerario de memoria        		4 Vaciar  datos de memoria 		  *\n"
				+ " *  5 Modificar un itinerario existente        		6 Guardar datos en el fichero 		  * \n"
				+ " *  7 Dividir los datos de memoria en dos ficheros      8 Mostrar contenido de memoria 	     	  *\n"
				+ " *  9 Mostrar Itinerarios de Menor a Mayor          	10 Mostrar Itinerarios de Mayor a menor   *\n"
				+ " *  11 Ver destino mas repetido          		12 Cerrar programa 			  * \n" 
				+" **************************************************************************************************"
				+ "\nOpcion: ");
		op = teclado.nextInt();

		return op;

	}

	/*******************************************/
	
	/********************************************/
	public void Crear_Menu(Scanner teclado) throws IOException, InterruptedException {
		int op;

		boolean fin = false;

		while (fin == false) {
			
			Thread.sleep(1500);
			op = opcion_seleccionada(teclado);

			switch (op) {
			case 1:
				// cargar datos de fichero en memoria
				leer_fichero(fichero);
				
				break;

			case 2:
				// añadir un itinerario a memoria
				Array_Itinerarios.add(insertar_itinerario(teclado));
				
				break;
			case 3:
				// borrar un itinerario de memoria
				borrar_itinerario(teclado);
				
				break;
			case 4:
				// vaciar memoria
				vaciar();
				
				break;
			case 5:
				// borra el itinerario indicado y lo define de nuevo con los nuevos datos del
				// usuario
				modificar_itinerario(teclado);
				
				break;
			case 6:
				guardar_datos_en_fichero();
				
				break;
			case 7:
				dividir_datos_enFicheros(teclado);
				
				break;
			case 8:
				// mostrar datos en memoria
				mostrar_contenido_array_Itinerarios();
				
				break;
			case 9:
				mostar_itinerarios_menorMayor();
				
				break;
			case 10:
				mostar_itinerarios_mayorMenor();
				
				break;
			case 11:
				ver_destino_mas_repetido();
				
				break;

			case 12:
				fin = true;
			
			}
		}

	}
	/*******************************************/

	/*********** Funciones Publicas *************/

	/*******************************************/

	public void leer_fichero(FicheroItinerarios fichero) throws IOException {

		fichero.Abrir(false, "Itinerarios.txt");

		while (fichero.Final() == false) {

			Array_Itinerarios.add(fichero.Leer());
		
		}
		System.out.println("Se ha llegado al final del fichero");

	}

	/*******************************************/

	/*******************************************/
	public Itinerario insertar_itinerario(Scanner teclado) throws IOException {

		int numero_destinos;
		ArrayList<String> Datos = new ArrayList<>();
		teclado.nextLine();

		System.out.print("Nombre del itinerario: ");
		Datos.add(0, teclado.nextLine());
		System.out.print("Numero de destinos: ");
		Datos.add(1, teclado.nextLine());

		numero_destinos = Integer.parseInt(Datos.get(1));

		for (int i = 0; i < numero_destinos; i++) {
			System.out.print("Destino " + (i + 1) + ": ");
			Datos.add(2 + i, teclado.nextLine());
		}

		Itinerario itinerario = new Itinerario(Datos);

		return itinerario;//@return retorna un objeto de tipo itinerario, relleno de Datos introducidos por el usuario

	}

	/*******************************************/

	/*******************************************/
	public void borrar_itinerario(Scanner teclado) {// borrara de memoria el itinerario indicado por el usuario

		teclado.nextLine();

		int comparacion;// variable que resuelve si nombre_itinerario_borrar es igual que el nombre de
						// algun itinerario cargado en memoria.
		String nombre_itinerario_borrar;

		System.out.println("Escriba el nombre del itinerario que desee borrar");
		System.out.print("Nombre: ");

		nombre_itinerario_borrar = teclado.nextLine();

		for (int i = 0; i < Array_Itinerarios.size(); i++) {// recorre array de objetos tipo itinerario
			for (int a = 0; a < Array_Itinerarios.get(i).getItinerario().size(); a++) {
				// recorre los datos del arraylist de dentro del objeto

				comparacion = nombre_itinerario_borrar.compareTo(Array_Itinerarios.get(i).getItinerario().get(a));
				// compara el nombre del itinerario introducido por el usuario con el nombre
				// de los itenerarios cargados en memoria
				
				if (comparacion == 0) {

					Array_Itinerarios.remove(i);
					System.out.println("Se borro el itinerario de nombre: " + nombre_itinerario_borrar);
					return;

				}

			}

		}

	}

	/*******************************************/

	/*******************************************/
	public void vaciar() throws IOException {

		Array_Itinerarios = new ArrayList<>();
		System.out.println("Los Datos fueron borrados de memoria");

	}

	/*******************************************/

	/********************************************/
	public void modificar_itinerario(Scanner teclado) throws IOException {
		String nombre_itinerario_modificar;
		int comparacion;
		teclado.nextLine();

		System.out.println("Introduzca el nombre del itinerario a modificar");
		System.out.print("nombre itinerario: ");
		nombre_itinerario_modificar = teclado.nextLine();

		for (int i = 0; i < Array_Itinerarios.size(); i++) {
			for (int a = 0; a < Array_Itinerarios.get(i).getItinerario().size(); a++) {//recorre todos los datos del array List de los objetos de tipo itinerario

				comparacion = nombre_itinerario_modificar.compareTo(Array_Itinerarios.get(i).getItinerario().get(a));
				//compara el nombre introducido con los nombres de los itinerarios en memoria
				//si el nombre coincide entra al if
				
				if (comparacion == 0) {
					System.out.println("Se va a modificar el itinerario de: " + nombre_itinerario_modificar);
					Array_Itinerarios.remove(i);
					System.out.println("Por favor inserte los nuevos datos");
					Array_Itinerarios.add(insertar_itinerario(teclado));
					return;
				}
			}

		}

	}

	/*******************************************/

	/********************************************/
	public void guardar_datos_en_fichero() throws IOException {
		
		fichero.Abrir(true, "Itinerarios.txt");//abre el fichero en modo escritura
		fichero.Escribir(Array_Itinerarios);
		fichero.Cerrar();

	}

	/*******************************************/

	/********************************************/
	public void dividir_datos_enFicheros(Scanner teclado) throws IOException {

		//gestion de creacion de los fichero que ha de crear este metodo
		File ficheromenores = new File("Menores.txt");
		FileWriter ficheroWmenores = new FileWriter(ficheromenores);
		BufferedWriter ficheroBWmenores = new BufferedWriter(ficheroWmenores);

		File ficheromayores = new File("Mayores.txt");
		FileWriter ficheroWMayores = new FileWriter(ficheromayores);
		BufferedWriter ficheroBWMayores = new BufferedWriter(ficheroWMayores);
		/*****************************************************/
		//variables de la funcion
		int destinosMax;
		int comparacion;
		teclado.nextLine();//vacio el buffer por si hubiera algo dentro
		
		System.out.println("Indique el numero de destinos maximo");
		System.out.print("Numero: ");
		destinosMax = teclado.nextInt();

		for (int i = 0; i < Array_Itinerarios.size(); i++) {
			comparacion = Integer.parseInt(Array_Itinerarios.get(i).getItinerario().get(1));//guarda el valor de numero de destinos en esta variable
			for (int a = 0; a < Array_Itinerarios.get(i).getItinerario().size(); a++) {//recorre todas las posiciones de Arraylist que tienen los objetos
				
				if (comparacion < destinosMax ) {
					ficheroBWmenores.write(Array_Itinerarios.get(i).getItinerario().get(a));
					ficheroBWmenores.newLine();
					ficheroBWmenores.flush();
					
					

				} else {
					ficheroBWMayores.write(Array_Itinerarios.get(i).getItinerario().get(a));
					ficheroBWMayores.newLine();
					ficheroBWMayores.flush();
					
					

				}
			}
		}
		ficheroBWmenores.close();
		ficheroBWMayores.close();

	}

	/*******************************************/

	/*******************************************/

	public void mostrar_contenido_array_Itinerarios() {

		for (int a = 0; a < Array_Itinerarios.size(); a++)
			System.out.println(Array_Itinerarios.get(a).toString());

		System.out.println("No hay mas itinerarios que mostrar");
	}

	/*******************************************/

	/*******************************************/
	public void mostar_itinerarios_menorMayor() {

		int menor = calcular_menor(), mayor = calcular_mayor();

		for (; menor <= mayor; menor++)
			for (int a = 0; a < Array_Itinerarios.size(); a++) {
				if (Integer.parseInt(Array_Itinerarios.get(a).getItinerario().get(1)) == menor)
					System.out.println(Array_Itinerarios.get(a).toString());
			}
	}

	/*******************************************/

	public void mostar_itinerarios_mayorMenor() {

		int menor = calcular_menor(), mayor = calcular_mayor();

		for (; mayor >= menor; mayor--)
			for (int a = 0; a < Array_Itinerarios.size(); a++) {
				if (Integer.parseInt(Array_Itinerarios.get(a).getItinerario().get(1)) == mayor)
					System.out.println(Array_Itinerarios.get(a).toString());
			}
	}

	/*******************************************/

	/*******************************************/
	public void ver_destino_mas_repetido() {
		
		String mas_repetido_real = "";
		String posible_mas_repetido = "";
		int veces_repetido = 0, buffer = 0;
		ArrayList<String> destinos = new ArrayList<>();

		for (int i = 0; i < Array_Itinerarios.size(); i++)
			for (int a = 0; a < Array_Itinerarios.get(i).getItinerario().size() - 2; a++)
				destinos.add(Array_Itinerarios.get(i).getItinerario().get(a + 2));

		System.out.println();

		for (int a = 0; a < destinos.size(); a++) {
			posible_mas_repetido = destinos.get(a);
			veces_repetido = 0;
			for (int j = a; j < destinos.size(); j++)
				if (posible_mas_repetido.equals(destinos.get(j)))
					veces_repetido++;

			if (veces_repetido > buffer) {
				mas_repetido_real = posible_mas_repetido;
				buffer = veces_repetido;

			}

		}

		System.out.println("Destino mas repetido es: " + mas_repetido_real);

	}

	/*******************************************/

	

	/*********** Funciones privadas *************/
	
	/*******************************************/
	private int calcular_menor() {
		int menor = 0;

		for (int i = 0; i < Array_Itinerarios.size() - 1; i++) {
			menor = Integer.parseInt((Array_Itinerarios.get(i).getItinerario().get(1)));
			if (menor > Integer.parseInt((Array_Itinerarios.get(i + 1).getItinerario().get(1))))
				menor = Integer.parseInt((Array_Itinerarios.get(i + 1).getItinerario().get(1)));

		}
		return menor;//@return devuelve el numero de destinos mas pequeño
	}

	private int calcular_mayor() {
		int mayor = 0;

		for (int i = 0; i < Array_Itinerarios.size() - 1; i++) {
			mayor = Integer.parseInt((Array_Itinerarios.get(i).getItinerario().get(1)));
			if (mayor < Integer.parseInt((Array_Itinerarios.get(i + 1).getItinerario().get(1))))
				mayor = Integer.parseInt((Array_Itinerarios.get(i + 1).getItinerario().get(1)));

		}
		return mayor;//@return devuelve el numero de destinos mas grande
	}
	/*******************************************/
	
	/*******************************************/
	@SuppressWarnings("unused")
	private void cerrar(FicheroItinerarios fichero) {

		try {
			fichero.Cerrar();
		} catch (IOException e) {

			System.out.println(e.getMessage());
		}
		System.out.print("El fichero se cerró correctamente ");
	}
	/*******************************************/
	
	/*******************************************/
	@SuppressWarnings("unused")
	private void abrir(FicheroItinerarios fichero, boolean leerOescribir) throws IOException {
		fichero.Abrir(leerOescribir, "Itinerarios.txt");
	}
	/*******************************************/
	
	
	/*******************************************/

}
