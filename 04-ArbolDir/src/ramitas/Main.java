package ramitas;

import java.io.*;

public class Main {

	public static void main(String[] args) throws IOException {

		String carpetaPadreN = "Proyecto";
		String arbol[][] = {

				{ "Estilos", "css4", "estilo1.css", "estilo2.css", "css3", "estilo3.css" },
				{ "Script", "js", "script1.js", "script2.js", "php", "cabecera.php", "modelo.php" },
				{ "Imagenes", "alta", "paisaje.jpg", "media", "paisaje.jpg", "baja", "paisaje.jpg" }

		};
		String ultimoDirectorio = "";

		File carpetaPadre = new File(carpetaPadreN);// objeto tipo file
		carpetaPadre.mkdir();// llamada el metodo crear carpeta de nombre = al objeto

		for (int i = 0; i < arbol.length; i++) {
			File ramas = new File(carpetaPadre, arbol[i][0]);
			ramas.mkdir();
			for (int j = 1; j < arbol[i].length; j++) {
				if (!arbol[i][j].contains(".")) {
					File carps = new File(carpetaPadreN + "/" + arbol[i][0], arbol[i][j]);
					carps.mkdir();
					ultimoDirectorio = arbol[i][j];
				} else {
					File fichs = new File(carpetaPadreN + "/" + arbol[i][0] + "/" + ultimoDirectorio, arbol[i][j]);
					fichs.createNewFile();

				}

			}
		}

		

	}

}
