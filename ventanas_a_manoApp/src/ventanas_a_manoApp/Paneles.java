package ventanas_a_manoApp;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Paneles extends JPanel{

	public Paneles(){
		setBackground(Color.white);
		setLayout(null);
		setBounds(0, 0, 1000, 400);
		
		JLabel labelPedirPalabra= new JLabel("Introduzca palabra a traducir");
		labelPedirPalabra.setBounds(10, 45, 200 ,30);
		//mensaje.setVisible(true);
		//palabra_a_traducir.setSize(palabra_a_traducir.getPreferredSize());
		
		JLabel labelTraduccion= new JLabel("Traducción: ");
		labelTraduccion.setBounds(10, 80, 200 ,30);
		//mensaje.setVisible(true);
		//traduccion.setSize(traduccion.getPreferredSize());
		
		JTextField palabra = new JTextField();
		palabra.setBackground(Color.ORANGE);
		palabra.setBounds(240, 45, 200, 30);
		
		add(labelPedirPalabra);
		add(labelTraduccion);
		add(palabra);
	}
}
