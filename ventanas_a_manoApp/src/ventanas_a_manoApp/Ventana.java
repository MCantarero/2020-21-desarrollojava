package ventanas_a_manoApp;
import javax.swing.JFrame; 


public class Ventana extends JFrame{

	//private String titulo;
	
	public Ventana(String title) {
		
		setVisible(true);
		setTitle(title);
		setResizable(true);
		setBounds(0, 0, 1000, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		Paneles primerPanel= new Paneles();
		
		add(primerPanel);
	}
	

	
}
