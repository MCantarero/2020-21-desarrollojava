package ejercicioFinal;

import java.util.Scanner;

public class menuPrincipal {
	
	public static FicheroItinerario objetoItinerario = new FicheroItinerario();
	public static MetodoItinerario metodoItinerario = new MetodoItinerario();
	public static String rutafichero = "VUELOS.txt";

	public static int menu() {
		@SuppressWarnings("resource")
		Scanner reader = new Scanner(System.in);
		int numero;

		System.out.println("AGENCIA DE VUELOS");
		System.out.println(" 1. Leer fichero\n"
				+ " 2. Insertar\n"
				+ " 3. Borrar\n"
				+ " 4. Vaciar\n"
				+ " 5. Modificar\n"
				+ " 6. Guardar fichero\n"
				+ " 7. Separar fichero\n"
				+ " 8. Mostrar contenido\n"
				+ " 9. Mostrar itinerario de menor a mayor\n"
				+ " 10. Mostrar itinerario de mayor a menor\n"
				+ " 11. Mostrar destino mas repetido\n"
				+ " 12. SALIR\n");
		

		numero = reader.nextInt();
		reader.nextLine();
		return numero;
	}
	
	public static void main(String[] args) throws Exception {

		Scanner reader;
		int opcion;

		reader = new Scanner(System.in);
		do {
			opcion = menu();
			switch (opcion) {

			case 1:
				Itinerario itinerario = new Itinerario();
				objetoItinerario.Leer(itinerario);
				break;
			case 2:
				metodoItinerario.Insertar();
				break;
			case 3:
				metodoItinerario.Borrar();
				break;
			case 4:
				metodoItinerario.VaciarArray();
				break;
			case 5:
				metodoItinerario.Modificar();
				break;
			case 6:
				metodoItinerario.GuardarFichero(rutafichero);
				break;
			case 7:
				metodoItinerario.SepararFichero();
				break;
			case 8:
				metodoItinerario.MostrarContenido();
				break;
			case 9:
				metodoItinerario.MenorMayor();
				break;
			case 10:
				metodoItinerario.MayorMenor();
				break;
			case 11:
				metodoItinerario.MasRepetido();
				break;
			default:
				System.out.println("Los números a introducir son del 1 al 11.");
				break;

			}

		} while (opcion != 12);
		System.out.println("Hasta pronto.");
		System.exit(0);
		reader.close();

	}

}
