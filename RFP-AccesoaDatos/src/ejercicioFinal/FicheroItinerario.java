package ejercicioFinal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class FicheroItinerario {

	final Scanner reader = new Scanner(System.in);
	private static MetodoItinerario metodoItinerario = new MetodoItinerario();

	private boolean abierto;
	private File ficherovuelos;
	private ArrayList<String> aux = new ArrayList<String>();
	public ArrayList<Itinerario> destino = new ArrayList<Itinerario>();

	public void Abrir(String rutas) {

		if (!abierto) {

			ficherovuelos = new File(rutas);

			if (!ficherovuelos.exists()) {

				try {
					ficherovuelos.createNewFile();
				} catch (IOException e) {

					e.printStackTrace();
				}

				System.out.println("+-----------------------+\n" + "| FICHERO NO ENCONTRADO |\n"
						+ "+-----------------------+\n\n");
				System.out.println("--> Fichero nuevo creado <--\n\n ");

				abierto = true;

			}

			else {
				abierto = true;

			}

		} else {
			System.out.println("El fichero está abierto");
		}
	}

	public void Leer(Itinerario nuevoItinerario) {
		
		Abrir(menuPrincipal.rutafichero);
		metodoItinerario.VaciarArray();
		if (this.abierto) {
			FileInputStream fs;
			try {

				fs = new FileInputStream(this.ficherovuelos.getPath());
				BufferedReader br = new BufferedReader(new InputStreamReader(fs));
				String linea = "";

				while ((linea = br.readLine()) != null) {
					
					String nombre = linea;
					int cantidad = Integer.parseInt(br.readLine());
					ArrayList<String> destiny = new ArrayList<String>();
					for (int i = 0; i < cantidad; i++)
						destiny.add(br.readLine());

					nuevoItinerario = new Itinerario(nombre, cantidad, destiny);
					this.destino.add(nuevoItinerario);

				}

				fs.close();
				br.close();
				
				Final();
				
			} catch (FileNotFoundException e) {
				System.err.println("Ups! Algo ha fallado...");
				System.err.println(e.getMessage());
				System.exit(0);
			} catch (IOException e) {
				System.err.println("Ups! Creo que esa informacion no iba ahi...");
				System.err.println(e.getMessage());
				System.exit(0);
			}
		} else
			System.err.println("Este fichero no esta abierto.");

	}

	public void Escribir(Itinerario iEscribir) {

		try {
			BufferedWriter bw;

			bw = new BufferedWriter(new FileWriter(menuPrincipal.rutafichero, true));

			bw.write(iEscribir.paraEscribir());

			bw.close();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void Cerrar() {

		System.out.println(aux);
		abierto = false;

	}

	public boolean Final() {

		if (abierto) {
			String line = "";
			int i = 1;
			try {
				@SuppressWarnings("resource")
				BufferedReader bf = new BufferedReader(new FileReader(menuPrincipal.rutafichero));

				while ((line = bf.readLine()) != null) {
					if (line != null) {
						System.out.println("Leyendo el registro --> " + (i) + "\n");
						System.out.println(line + "\n");
						aux.add(line);
						i++;
					}

				}

			} catch (IOException e) {
				System.out.println("ERROR");
			}
			
		} else {
			System.out.println("El fichero no esta abierto.");
		}
		
		System.out.println("\n\nHa terminado la lectura\n\n");
		return false;
	}

	public void Vaciar(String ruta) {

		try {
			ficherovuelos = new File(ruta);
			BufferedWriter bw = new BufferedWriter(new FileWriter(ficherovuelos));

			bw.write("");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
		
}
