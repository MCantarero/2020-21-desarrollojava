package ejercicioFinal;

import java.util.ArrayList;

public class Itinerario {

	private String nombre;
	private ArrayList<String> destino;
	private int cantidad;
	
	public Itinerario() {

	}


	public Itinerario(String nombre, int cantidad, ArrayList<String> destino) {
		super();
		this.nombre = nombre;
		this.destino = destino;
		this.cantidad = cantidad;

	}

	public String getNombre() {
		return nombre;
	}

	public ArrayList<String> getDestino() {
		return destino;
	}

	public int getCantidad() {
		return cantidad;
	}

	@Override
	public String toString() {
		return "+--------------+\n" + "|| ITINERARIO ||\n" + "+--------------+\n" + nombre + "\n" + cantidad + "\n"
				+ destino;
	}

	public String paraEscribir() {
		String aux = "";
		for (int i = 0; i < this.destino.size(); i++)
			aux += this.destino.get(i) + "\n";
		
		return nombre + "\n" + cantidad + "\n" + aux;
	}

}