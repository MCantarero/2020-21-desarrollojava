package ejercicioFinal;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class MetodoItinerario {
	
	final Scanner reader = new Scanner(System.in);
	private FicheroItinerario objetoItinerario = new FicheroItinerario();
	
	public void Insertar() {

		String nombre;
		int cantidad;
		
		try {
			ArrayList<String> auxEscribir = new ArrayList<String>();
			System.out.println("NOMBRE --> ");
			nombre = reader.nextLine();

			System.out.println("\nCANTIDAD --> ");
			cantidad = reader.nextInt();
			reader.nextLine();

			for (int i = 0; i < cantidad; i++) {
				System.out.println("\nDestinos --> ");
				auxEscribir.add(reader.nextLine());
			}
			Itinerario iEscribir = new Itinerario(nombre, cantidad, auxEscribir);
			this.objetoItinerario.destino.add(iEscribir);
		} catch (Exception e) {
			System.out.print("Debe ser un caracter numérico");
			Insertar();
		}

	}
	
	public void auxiliarE(Itinerario iEscribir, String rutas) {

		objetoItinerario.Abrir(rutas);

		try {
			BufferedWriter bw;

			bw = new BufferedWriter(new FileWriter(rutas, true));

			bw.write(iEscribir.paraEscribir());

			bw.close();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	
	public void VaciarArray() {
		this.objetoItinerario.destino.clear();
	}

	public void Borrar() throws Exception {

		int pos = 0;

		MostrarContenido();

		System.out.println("Diga el que quiera borrar --> ");

		pos = reader.nextInt();

		reader.nextLine();

		for (int j = 0; j < this.objetoItinerario.destino.size(); j++) {
			if ((pos - 1) == j)
				this.objetoItinerario.destino.remove(j);

		}
	}

	public void Modificar() throws Exception{
		
		MostrarContenido();

		System.out.println("Diga el que quiera modificar --> ");
		int pos = reader.nextInt();
		
		if(pos > this.objetoItinerario.destino.size() || pos <= 0) {
			System.out.println("Debe contener los números del 1 al " + pos);
			Modificar();
		} else {
			
		ArrayList<String> modify = new ArrayList<String>();

		reader.nextLine();

		System.out.println("NOMBRE --> ");
		String name = reader.nextLine();

		System.out.println("\nCANTIDAD --> ");
		int c = reader.nextInt();

		reader.nextLine();

		for (int i = 0; i < c; i++) {
			System.out.println("\nDestinos --> ");
			modify.add(reader.nextLine());
		}

		Itinerario mod = new Itinerario(name, c, modify);

		this.objetoItinerario.destino.set(pos - 1, mod);
		
		}

	}

	public void GuardarFichero(String rutas) {

		objetoItinerario.Vaciar(rutas);
		for (int i = 0; i < this.objetoItinerario.destino.size(); i++) {
			objetoItinerario.Escribir(this.objetoItinerario.destino.get(i));

		}

	}

	public void MostrarContenido() {

		for (int i = 0; i < this.objetoItinerario.destino.size(); i++)
			System.out.println("-----" + (i + 1) + "-----\n" + this.objetoItinerario.destino.get(i).toString() + "\n\n");

	}

	public void SepararFichero() {

		String fichero1 = "MENORES.txt", fichero2 = "IGUALMAYORES.txt";

		System.out.println("Elija un numero para separarlos en menores o iguales y mayores:");
		int numRef = reader.nextInt();

		objetoItinerario.Abrir(fichero1);
		objetoItinerario.Abrir(fichero2);

		for (int sf = 0; sf < this.objetoItinerario.destino.size(); sf++) {

			if ((this.objetoItinerario.destino.get(sf).getCantidad()) < numRef) {

				auxiliarE(this.objetoItinerario.destino.get(sf), fichero1);

			} else

				auxiliarE(this.objetoItinerario.destino.get(sf), fichero2);

		}
	}

	public void MenorMayor() {

		int numMin = 1000;

		for (int i = 0; i < this.objetoItinerario.destino.size(); i++)
			if (this.objetoItinerario.destino.get(i).getCantidad() < numMin)
				numMin = this.objetoItinerario.destino.get(i).getCantidad();

		for (int j = numMin; j <= 1000; j++)
			for (int k = 0; k < this.objetoItinerario.destino.size(); k++)
				if (this.objetoItinerario.destino.get(k).getCantidad() == j)
					System.out.println(this.objetoItinerario.destino.get(k).toString() + "\n");

	}

	public void MayorMenor() {

		int numMax = -1;

		for (int i = 0; i < this.objetoItinerario.destino.size(); i++)
			if (this.objetoItinerario.destino.get(i).getCantidad() > numMax)
				numMax = this.objetoItinerario.destino.get(i).getCantidad();

		for (int j = numMax; j >= -1000; j--)
			for (int k = 0; k < this.objetoItinerario.destino.size(); k++)
				if (this.objetoItinerario.destino.get(k).getCantidad() == j)
					System.out.println(this.objetoItinerario.destino.get(k).toString() + "\n");

	}

	public void MasRepetido() {

		ArrayList<String> masRep = new ArrayList<String>();
		int repetido = 0, idDelRepetido = 0;
		
		for (int i = 0; i < this.objetoItinerario.destino.size(); i++)
			for (int j = 0; j < this.objetoItinerario.destino.get(i).getDestino().size(); j++)
				masRep.add(this.objetoItinerario.destino.get(i).getDestino().get(j));

		for (int i = 0; i < masRep.size(); i++)
			if (Collections.frequency(masRep, masRep.get(i)) > repetido) {
				repetido = Collections.frequency(masRep, masRep.get(i));
				idDelRepetido = i;
			}

		System.out.println(masRep.get(idDelRepetido));

	}
}
