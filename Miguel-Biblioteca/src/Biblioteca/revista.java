package Biblioteca;

public class revista extends cInfo {
	int numero;

	public revista(int codigo, int anio, String titulo, int numero) {
		super(codigo, anio, titulo);
		this.numero = numero;
	}


	public String showyear(revista R) {

		return Integer.toString(R.getFyear());
	}
	
	public String showcode(revista R) {

		return Integer.toString(R.getCod());
	}

	
	@Override
	public String toString() {
		return  "Revista - T�tulo: " + super.getTitle() +" A�o de Publicaci�n: " + super.getFyear() +" C�digo: "+ super.getCod() +" N�mero: " + numero;
	}
	

}
