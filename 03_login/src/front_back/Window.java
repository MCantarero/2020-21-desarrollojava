package front_back;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import javax.swing.JButton;
import net.miginfocom.swing.MigLayout;
import java.awt.BorderLayout;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JTextPane;
import javax.swing.JSpinner;
import javax.swing.JList;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.AbstractListModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class Window {

	private JFrame frame;
	private JPasswordField passwordField;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.GRAY);
		
		frame.setBounds(100, 100, 850, 650);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(2, 4, 5, 5));
		
		/******************************************************/
		//Apariencia del Panel
		JPanel Login = new JPanel();
		Login.setBackground(new Color(255, 255, 153));
		frame.getContentPane().add(Login);
		Login.setLayout(null);
		
		
		//Botones
		JButton Boton_Login = new JButton("Iniciar Sesi\u00F3n");
		Boton_Login.setFont(new Font("Monospaced", Font.BOLD, 16));
		Boton_Login.setBounds(209, 234, 195, 30);
		Login.add(Boton_Login);
		
		JButton Boton_registro = new JButton("Registrarse");
		Boton_registro.setFont(new Font("Monospaced", Font.BOLD, 16));
		Boton_registro.setBounds(10, 234,178, 30);
		Login.add(Boton_registro);
		
		//Campos de Texto
		passwordField = new JPasswordField();
		passwordField.setBounds(126, 177, 160, 25);
		Login.add(passwordField);
		
		textField = new JTextField();
		textField.setBounds(126, 103, 160, 25);
		Login.add(textField);
		textField.setColumns(10);
		
		
		//Labels
		JLabel label_nombre = new JLabel("Nombre de Usuario");
		label_nombre.setBackground(new Color(255, 255, 255));
		label_nombre.setForeground(new Color(0, 0, 0));
		label_nombre.setFont(new Font("Monospaced", Font.BOLD, 16));
		label_nombre.setBounds(126, 62, 175, 30);
		Login.add(label_nombre);
		
		JLabel label_passwd = new JLabel("Contrase\u00F1a");
		label_passwd.setForeground(Color.BLACK);
		label_passwd.setFont(new Font("Monospaced", Font.BOLD, 16));
		label_passwd.setBackground(Color.WHITE);
		label_passwd.setBounds(153, 136, 105, 30);
		Login.add(label_passwd);
		
		
		/******************************************************/
		
		/******************************************************/
		JPanel Configuracion = new JPanel();
		Configuracion.setBackground(new Color(51, 204, 102));
		frame.getContentPane().add(Configuracion);
		Configuracion.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(82, 53, 266, 207);
		Configuracion.add(panel);
		panel.setLayout(null);
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("New check box");
		chckbxNewCheckBox_1.setFont(new Font("Monospaced", Font.BOLD, 16));
		chckbxNewCheckBox_1.setBounds(35, 49, 155, 23);
		panel.add(chckbxNewCheckBox_1);
		
		JCheckBox chckbxNewCheckBox_1_1 = new JCheckBox("New check box");
		chckbxNewCheckBox_1_1.setFont(new Font("Monospaced", Font.BOLD, 16));
		chckbxNewCheckBox_1_1.setBounds(35, 75, 182, 23);
		panel.add(chckbxNewCheckBox_1_1);
		
		JCheckBox chckbxNewCheckBox_1_2 = new JCheckBox("New check box");
		chckbxNewCheckBox_1_2.setFont(new Font("Monospaced", Font.BOLD, 16));
		chckbxNewCheckBox_1_2.setBounds(35, 105, 182, 23);
		panel.add(chckbxNewCheckBox_1_2);
		
		JLabel label_nombre_1 = new JLabel("Configuraci\u00F3n");
		label_nombre_1.setForeground(Color.BLACK);
		label_nombre_1.setFont(new Font("Monospaced", Font.BOLD, 16));
		label_nombre_1.setBackground(Color.WHITE);
		label_nombre_1.setBounds(51, 11, 139, 30);
		panel.add(label_nombre_1);
		
		JButton Boton_Login_1 = new JButton("Validar");
		Boton_Login_1.setFont(new Font("Monospaced", Font.BOLD, 16));
		Boton_Login_1.setBounds(35, 166, 195, 30);
		panel.add(Boton_Login_1);
		
	
	
		
		/******************************************************/
		
		/******************************************************/
		JPanel Vista_datos = new JPanel();
		Vista_datos.setBackground(new Color(102, 51, 255));
		frame.getContentPane().add(Vista_datos);
		/******************************************************/
		
		/******************************************************/
		JPanel Final = new JPanel();
		Final.setBackground(new Color(0, 153, 255));
		frame.getContentPane().add(Final);
	}
}
