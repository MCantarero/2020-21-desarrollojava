package files_and_more;

import java.io.IOException;

public class Main {
	
	
	public void escribirFichero(String nombre,String []tabla) throws IOException 
	{
		
			// 1. Creaci�n del fichero
		    File fichero = new File(nombre);
		    FileWriter ficheroFW = new FileWriter(fichero);
		    BufferedWriter ficheroBW = new BufferedWriter(ficheroFW);
		    
		    // 2. Escribir datos en el fichero
		    for(int i=0;i<tabla.length;i++)
		    {
		    	ficheroBW.write(tabla[i]);
			    ficheroBW.newLine();		    	
		    }
		    		    
		    // 3. Cerrar el fichero
		    ficheroBW.close(); 
	}
	
	public  void leerFichero(String nombre, String[] tabla)
	{
		try
		{
			// 1. Creaci�n dle fichero para leer
			File fichero = new File(nombre);
			FileReader ficherorR = new FileReader(fichero);
			BufferedReader ficheroBR = new BufferedReader(ficherorR);
			int posicion=0;
			String linea="";
			
			// 2. Lectura del fichero
			linea = ficheroBR.readLine();
			while (linea!=null)
			{
			  tabla[posicion] = linea;
			  posicion++; 
   			  linea = ficheroBR.readLine();
			}
			ficheroBR.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	

	
	public  void imprimirPantalla(String[] tabla)
	{
		System.out.println("El contenido de la tabla es: ");
		for(int i=0;i<tabla.length;i++)
		{
			System.out.print(tabla[i] + " - ");
		}
	}
	
	public  void vaciarTabla(String[] tabla)
	{
		for(int i=0;i<tabla.length;i++)
		{
			tabla[i] = "";
		}
	}

	public static void main(String[] args)  {
		// TODO Auto-generated method stub

		
		
		
		
		 String[] ArrayData = { "Alex", "Pablo", "Nacho", "Jose Carlos" };
		 ArrayNames Newfile=new ArrayNames();
		 
		 Newfile.crearFichero("fichero1");
			
			// Llamada a escribir fichero
			// escribirFichero("PruebaNombres.txt",tablaNombres);

			// Vaciar la tabla
			//vaciarTabla(tablaNombres);
			//imprimirPantalla(tablaNombres);
			
			//leerFichero("PruebaNombres.txt",tablaNombres);
			//imprimirPantalla(tablaNombres);
			
			System.out.println("Fin el programa");
			
		}

	}


